package at.myflexbox.lockerservice.impl.converter;


import at.myflexbox.lockerservice.core.domain.CompartmentDTO;
import at.myflexbox.lockerservice.persistence.entity.Compartment;

public class CompartmentConverter {
    public static CompartmentDTO convertToCompartmentDto(Compartment compartment) {
        return new CompartmentDTO(compartment.getNumber());
    }
}
