package at.myflexbox.lockerservice.impl.controller;

import at.myflexbox.lockerservice.core.domain.CompartmentDTO;

import java.util.List;

public interface CompartmentControllerInterface {
    List<CompartmentDTO> getByLockerId(Long lockerId);
}
