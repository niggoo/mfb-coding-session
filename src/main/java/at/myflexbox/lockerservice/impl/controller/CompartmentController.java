package at.myflexbox.lockerservice.impl.controller;

import at.myflexbox.lockerservice.core.domain.CompartmentDTO;
import at.myflexbox.lockerservice.impl.converter.CompartmentConverter;
import at.myflexbox.lockerservice.persistence.CompartmentRepository;
import at.myflexbox.lockerservice.persistence.entity.Compartment;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class CompartmentController implements CompartmentControllerInterface {
    private final CompartmentRepository compartmentRepository;

    @Override
    public List<CompartmentDTO> getByLockerId(Long lockerId) {
        List<Compartment> compartments = compartmentRepository.findByLockerId(lockerId);
        return compartments.stream()
                .map(CompartmentConverter::convertToCompartmentDto)
                .toList();
    }
}
