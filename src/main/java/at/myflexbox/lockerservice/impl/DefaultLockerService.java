package at.myflexbox.lockerservice.impl;

import at.myflexbox.lockerservice.core.LockerService;
import at.myflexbox.lockerservice.core.domain.CompartmentDTO;
import at.myflexbox.lockerservice.impl.controller.CompartmentControllerInterface;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class DefaultLockerService implements LockerService {

    private final CompartmentControllerInterface compartmentController;

    @Override
    public List<CompartmentDTO> getCompartmentsByLockerId(Long lockerId) {
        return compartmentController.getByLockerId(lockerId);
    }
}
