package at.myflexbox.lockerservice.init;

import at.myflexbox.lockerservice.persistence.CompartmentRepository;
import at.myflexbox.lockerservice.persistence.entity.Compartment;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class DatabaseInitializer implements ApplicationRunner {

    private final CompartmentRepository compartmentRepository;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("Initializing database");


        compartmentRepository.save(new Compartment(1L, 1L, 1L));
        compartmentRepository.save(new Compartment(2L, 2L, 1L));
        compartmentRepository.save(new Compartment(3L, 3L, 1L));
        compartmentRepository.save(new Compartment(4L, 4L, 1L));
        compartmentRepository.save(new Compartment(5L, 5L, 1L));
    }
}
