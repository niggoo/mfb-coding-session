package at.myflexbox.lockerservice.persistence;

import at.myflexbox.lockerservice.persistence.entity.Compartment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompartmentRepository extends JpaRepository<Compartment, Long> {
    List<Compartment> findByLockerId(Long lockerId);
}
