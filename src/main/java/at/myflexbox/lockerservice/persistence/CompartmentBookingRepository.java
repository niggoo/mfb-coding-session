package at.myflexbox.lockerservice.persistence;

import at.myflexbox.lockerservice.persistence.entity.CompartmentBooking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompartmentBookingRepository extends JpaRepository<CompartmentBooking, Long> {
}
