package at.myflexbox.lockerservice.core;

import at.myflexbox.lockerservice.core.domain.CompartmentDTO;

import java.util.List;

public interface LockerService {
    List<CompartmentDTO> getCompartmentsByLockerId(Long lockerId);

}
