package at.myflexbox.lockerservice.core.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class CompartmentDTO {
    private Long number;
}
