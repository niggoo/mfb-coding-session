package at.myflexbox.lockerservice.api;

import at.myflexbox.lockerservice.core.LockerService;
import at.myflexbox.lockerservice.core.domain.CompartmentDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class LockerApi {
    private final LockerService lockerService;

    @GetMapping("locker/{lockerId}/compartments")
    public List<CompartmentDTO> getCompartmentsByLockerId(@PathVariable("lockerId") Long lockerId) {
        return lockerService.getCompartmentsByLockerId(lockerId);
    }
}
