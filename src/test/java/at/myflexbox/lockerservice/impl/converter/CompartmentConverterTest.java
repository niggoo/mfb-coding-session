package at.myflexbox.lockerservice.impl.converter;

import at.myflexbox.lockerservice.core.domain.CompartmentDTO;
import at.myflexbox.lockerservice.persistence.entity.Compartment;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CompartmentConverterTest {

    @Test
    void testConverToCompartmentDto() {
        Compartment comp = new Compartment(12L, 7L, 81L);

        CompartmentDTO result = CompartmentConverter.convertToCompartmentDto(comp);
        assertEquals(7L, result.getNumber());
    }

}