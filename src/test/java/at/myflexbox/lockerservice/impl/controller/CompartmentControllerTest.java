package at.myflexbox.lockerservice.impl.controller;

import at.myflexbox.lockerservice.core.domain.CompartmentDTO;
import at.myflexbox.lockerservice.persistence.CompartmentRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


class CompartmentControllerTest {
    @Test
    void getByLockerId() {
        long lockerId = 12L;

        CompartmentRepository compartmentRepositoryMock = Mockito.mock(CompartmentRepository.class);
        Mockito.when(compartmentRepositoryMock.findByLockerId(lockerId)).thenReturn(Collections.emptyList());

        CompartmentController compartmentController = new CompartmentController(compartmentRepositoryMock);
        List<CompartmentDTO> result = compartmentController.getByLockerId(lockerId);
        assertEquals(Collections.emptyList(), result);
    }
}