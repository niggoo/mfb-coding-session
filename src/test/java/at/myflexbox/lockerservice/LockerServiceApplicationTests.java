package at.myflexbox.lockerservice;

import at.myflexbox.lockerservice.core.domain.CompartmentDTO;
import at.myflexbox.lockerservice.init.DatabaseInitializer;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class LockerServiceApplicationTests {

    @Autowired
    private TestRestTemplate restTemplate;

    @SpyBean
    DatabaseInitializer initializer;

    @Test
    void contextLoads() throws Exception {
        Mockito.verify(initializer, times(1)).run(any());
    }

    @Test
    void testGetCompartmentsByLockerId() {
        Map<String, String> params = new HashMap<>();
        params.put("lockerId", "12");

        ResponseEntity<List<CompartmentDTO>> result = restTemplate.exchange("/locker/{lockerId}/compartments", HttpMethod.GET, null,
                new ParameterizedTypeReference<>() {
                }, params);

        assertEquals(5, result.getBody().size());
    }
}
